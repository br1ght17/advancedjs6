document.getElementById("findButton").addEventListener("click", async function() {
    try {
        const ipResponse = await fetch("https://api.ipify.org/?format=json");
        const ipData = await ipResponse.json();
        const ipAddress = ipData.ip;
        console.log(ipAddress);
        const geoResponse = await fetch(`http://ip-api.com/json/${ipAddress}`);
        const geoData = await geoResponse.json();

        const infoDiv = document.getElementById("info");
        infoDiv.innerHTML = `
            <p>Континент: ${geoData.timezone.split('/')[0]}</p>
            <p>Країна: ${geoData.country}</p>
            <p>Регіон: ${geoData.region}</p>
            <p>Місто: ${geoData.city}</p>
            <p>Район номер: ${geoData.district}</p>
        `;
    } catch (error) {
        console.error("Помилка: ", error);
    }
});